package com.nastsin.ec.springweb.mapper;

import com.nastsin.ec.springweb.dto.UserDto;
import com.nastsin.ec.springweb.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UsersMapper {

    private final ModelMapper mapper;

    @Autowired
    public UsersMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public User toEntity(UserDto addressDto) {
        User user = Objects.isNull(addressDto) ? null : mapper.map(addressDto, User.class);
        if(user != null){
            user.getAddress().setCity(addressDto.getCity());
            user.getAddress().setStreet(addressDto.getStreet());
            user.getAddress().setUser(user);
        }
        return user;
    }

    public UserDto toDto(User entity) {
        UserDto userDto = Objects.isNull(entity) ? null : mapper.map(entity, UserDto.class);
        if(userDto != null && entity.getAddress() != null){
            userDto.setCity(entity.getAddress().getCity());
            userDto.setStreet(entity.getAddress().getStreet());
        }
        return userDto;
    }
}
