package com.nastsin.ec.springweb.web.controller;

import com.nastsin.ec.springweb.dto.ItemDto;
import com.nastsin.ec.springweb.entity.Item;
import com.nastsin.ec.springweb.mapper.ItemMapper;
import com.nastsin.ec.springweb.exception.NotFoundException;
import com.nastsin.ec.springweb.service.DefaultItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/items")
public class ItemController {
    private final DefaultItemService itemService;
    private final ItemMapper mapper;

    @Autowired
    public ItemController(DefaultItemService itemService, ItemMapper mapper) {
        this.itemService = itemService;
        this.mapper = mapper;
    }


    @GetMapping("/{id}")
    public ResponseEntity<ItemDto> findItem(@PathVariable("id") Long id) {
        Item item = itemService.getItem(id);
        ItemDto itemDto = mapper.toDto(item);
        return new ResponseEntity<>(itemDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ItemDto> saveItem(@RequestBody ItemDto itemDto) {
        Item item = mapper.toEntity(itemDto);
        return saveItem(item);
    }

    private ResponseEntity<ItemDto> saveItem(Item item){
        Item savedItem = itemService.saveItem(item);
        ItemDto savedItemDto = mapper.toDto(savedItem);
        return new ResponseEntity<>(savedItemDto, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ItemDto> updateItem(@RequestBody ItemDto itemDto) throws NotFoundException {
        Item item = mapper.toEntity(itemDto);
        itemService.getItem(item.getId());
        return saveItem(item);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ItemDto> deleteItem(@PathVariable("id") Long id) {
        Item item = itemService.getItem(id);
        itemService.deleteItem(id);
        ItemDto itemDto = mapper.toDto(item);
        return new ResponseEntity<>(itemDto, HttpStatus.OK);
    }


}
