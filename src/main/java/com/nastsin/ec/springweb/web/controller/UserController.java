package com.nastsin.ec.springweb.web.controller;

import com.nastsin.ec.springweb.dto.UserDto;
import com.nastsin.ec.springweb.entity.User;
import com.nastsin.ec.springweb.mapper.UsersMapper;
import com.nastsin.ec.springweb.service.DefaultUserService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nastsin.ec.springweb.entity.Item;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.nastsin.ec.springweb.exception.NotFoundException;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final DefaultUserService userService;
    private final UsersMapper mapper;

    @Autowired
    public UserController(DefaultUserService userService, UsersMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }


    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findUser(@PathVariable("id") Long id) {
        User user = userService.getUser(id);
        System.out.println(user);
        UserDto userDto = mapper.toDto(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto) {
        User user = mapper.toEntity(userDto);
        return saveUser(user);
    }

    @PutMapping
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto) throws NotFoundException {
        User user = mapper.toEntity(userDto);
        userService.getUser(user.getId());
        return saveUser(user);
    }

    private ResponseEntity<UserDto> saveUser(User user){
        User savedUser = userService.saveUser(user);
        UserDto savedUserDto = mapper.toDto(savedUser);
        return new ResponseEntity<>(savedUserDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable("id") Long id) {
        User user = userService.getUser(id);
        for(Item elem: user.getItems()){
            elem.setUser(null);
        }
        userService.deleteUser(id);
        UserDto userDto = mapper.toDto(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }


}
